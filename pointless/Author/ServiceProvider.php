<?php

namespace Pointless\Author;

use Illuminate\Database\Schema\Blueprint;
use Pointless\Author\Blueprint as AuthorBlueprint;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider
{

    public function register()
    {
    	Blueprint::mixin(new AuthorBlueprint);
    }

}

<?php

namespace Pointless\Repositories\Supports;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Builder;

class RawFilterWhereClause
{
    protected $query;

    public function __construct(Builder $query)
    {
        $this->query = $query;
    }

    public function apply($attribute, $raw)
    {
        if (strpos($attribute, '.') !== false) {
            [$relation, $attribute] = explode('.', $attribute, 2);
            $this->query->whereHas($relation, function ($related) use ($attribute, $raw) {
                (new static($related))->apply($attribute, $raw);
            });
        } else {
            is_array($raw) ? 
                $this->applyArrayWhereClause($attribute, $raw) : 
                $this->applyStringWhereClause($attribute, $raw);
        }
        return $this;
    }

    public static function trimLiteral($value)
    {
        return str_is('`*`', $value) ? trim($value, '`') : $value;
    }

    public static function isDate($value)
    {
        return rescue(function () use ($value) {
            return Carbon::createFromFormat('Y-m-d', $value) == true;
        }, false);
    }

    protected function applyArrayWhereClause($attribute, array $raw)
    {
        [$notInValues, $inValues] = collect($raw)->partition(function ($value) {
            return starts_with($value, '!');
        });

        $notInValues = $notInValues->map(function ($value) {
            return $this->trimLiteral(substr($value, 1));
        })->all();

        $inValues = $inValues->map([$this, 'trimLiteral'])->all();

        if ($notInValues) $this->query->whereNotIn($attribute, $notInValues);
        if ($inValues) $this->query->whereIn($attribute, $inValues);
    }

    protected function applyStringWhereClause($attribute, $raw)
    {
        if (strlen($raw) === 0) {
            $this->query->whereNotNull($attribute);
        } else if ($raw === '!') {
            $this->query->whereNull($attribute);
        } else if (starts_with($raw, ['>=', '<='])) {
            $this->query->where($attribute, substr($raw, 0, 2), $this->trimLiteral(substr($raw, 2)));
        } else if (starts_with($raw, ['>', '<'])) {
            $this->query->where($attribute, substr($raw, 0, 1), $this->trimLiteral(substr($raw, 1)));
        } else if (starts_with($raw, '!')) {
            $this->query->where($attribute, '!=', $this->trimLiteral(substr($raw, 1)));
        } else if (starts_with($raw, '~')) {
            $this->query->where($attribute, 'ilike', '%' . $this->trimLiteral(substr($raw, 1)) . '%');
        } else if (starts_with($raw, '^')) {
            $this->query->where($attribute, 'ilike', $this->trimLiteral(substr($raw, 1)) . '%');
        } else if (starts_with($raw, '$')) {
            $this->query->where($attribute, 'ilike', '%' . $this->trimLiteral(substr($raw, 1)));
        } else if (str_is('*|*', $raw) && ! str_is('`*`', $raw)) {
            $this->applyBetweenWhereClause($attribute, ...explode('|', $raw, 2));
        } else {
            $raw = $this->trimLiteral($raw);
            if ($this->isDate($raw)) {
                $this->query->whereDate($attribute, $raw);
            } else {
                $this->query->where($attribute, $raw);
            }
        }
    }

    protected function applyBetweenWhereClause($attribute, $from, $to)
    {
        $from = $this->trimLiteral($from);
        $to = $this->trimLiteral($to);
        if ($from === '') {
            $this->query->where($attribute, '<=', $to);
        } else if ($to === '') {
            $this->query->where($attribute, '>=', $from);
        } else {
            if ($this->isDate($from) && $this->isDate($to)) {
                $this->query->whereDate($attribute, '>=', $from)->whereDate($attribute, '<=', $to);
            } else {
                $this->query->whereBetween($attribute, [$from, $to]);
            }
        }
    }

}
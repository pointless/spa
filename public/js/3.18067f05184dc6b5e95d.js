webpackJsonp([3],{

/***/ 69:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = null
/* template */
var __vue_template__ = __webpack_require__(73)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/views/Home.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6c0a33b2", Component.options)
  } else {
    hotAPI.reload("data-v-6c0a33b2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 73:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "flex-grow bg-white py-8" }, [
      _c("div", { staticClass: "container mx-auto px-4" }, [
        _c("p", { staticClass: "border p-2 my-4 font-normal" }, [
          _vm._v("\r\n            Upload and annotate "),
          _c("em", [_vm._v("any document")]),
          _vm._v(
            ", verify your identity, \r\n            and connect with an eNotary in minutes. "
          ),
          _c(
            "a",
            {
              staticClass: "hyperlink",
              attrs: {
                href:
                  "?alert=All handled securely through \r\n            the Notarize app. Once done, print or share your notarized document \r\n            electronically."
              }
            },
            [
              _vm._v(
                "All handled securely through \r\n            the Notarize app. Once done, print or share your notarized document \r\n            electronically."
              )
            ]
          ),
          _vm._v(" "),
          _c("span", { staticClass: "font-semibold" }, [
            _vm._v("The rise of single-page websites")
          ]),
          _vm._v(
            " \r\n            has driven many new trends into the limelight. One of these is dot navigation: \r\n            a series of circular icons located on the left or right side of the screen. \r\n            Each of these dots represents a different section of the site. And since the \r\n            layout is one long page, these links are highlighted to indicate the user’s current \r\n            position. I really like the ingenuity of this trend, but there is a question \r\n            mark over if people understand what these dots stand for or how to use them. \r\n            Why use this?\r\n            If you’re designing a single page layout then I’d recommend using a sticky navbar \r\n            at the top of the page. This makes it easier to see what each slide represents and \r\n            what sort of information is on the page.\r\n            But if you can’t (or don’t want to) use a top navigation then these dot features \r\n            are the next best thing. Or even better: combine both! It’s possible to have a \r\n            top nav with text links and include dot navigation icons too. Or even include \r\n            text beside the dots. There are lots of options to choose from, and they can \r\n            all work well.\r\n        "
          )
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6c0a33b2", module.exports)
  }
}

/***/ })

});
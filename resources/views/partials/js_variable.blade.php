<script>
window.app = {!! json_encode([
	'now' => now()->toIso8601String(),
	'vuedata' => (object) app('vuedata'),
	'csrfToken' => csrf_token(),
	'notifications' => array_except($errors->getBags(), ['validation']),
	'validationErrors' => $errors->getBag('validation'),
]) !!}
</script>
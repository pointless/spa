<?php

namespace Pointless\Restful\Http\Traits;

use Pointless\Restful\Http\JsonResponse;

trait ReturnsJsonResponse
{

    public function success($message = null, $data = null, $statusCode = 200)
    {
        return JsonResponse::success($message, $data, $statusCode);
    }
    
    public function fail($message = null, $data = null, $statusCode = 412)
    {
        return JsonResponse::fail($message, $data, $statusCode);
    }

}
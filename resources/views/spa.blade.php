<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }}</title>
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">

    @stack('styles')
</head>
<body class="bg-grey-lightest text-black font-sans font-normal leading-normal">
    
    <div id="app" class="min-h-screen flex flex-col pt-14 md:pt-20" v-cloak>
        <app></app>
    </div>

    @include('partials.js_variable')
    @stack('pre-scripts')
    <script src="{{ asset('js/app.js') }}"></script>
    @stack('scripts')
</body>
</html>

import router from '../router'


export function matchUrlPattern(pattern, url) {
    const regex = new RegExp("^" + pattern.split("*").join("[^\/]+") + "[\/]?$")
    return regex.test(url)
}

export function redirect(location) {
    if (!location) return
    if (location === true) return window.location.reload()
    return router.push(location)
}

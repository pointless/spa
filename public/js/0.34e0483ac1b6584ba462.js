webpackJsonp([0],{

/***/ 93:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(94)
/* template */
var __vue_template__ = __webpack_require__(95)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\views\\ClientHome.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-fb5c83d2", Component.options)
  } else {
    hotAPI.reload("data-v-fb5c83d2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_GlobalSearch__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_GlobalSearch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_GlobalSearch__);


function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    components: { GlobalSearch: __WEBPACK_IMPORTED_MODULE_1__components_GlobalSearch___default.a },
    data: function data() {
        return {
            scroll: 0,
            sticking: true
        };
    },

    methods: {
        toggleNavbar: function toggleNavbar(state) {
            document.body.classList.toggle('navbar-open', state);
        },
        stickHeader: function stickHeader() {
            var currentScroll = window.pageYOffset;
            if (currentScroll < this.scroll) {
                this.sticking = true;
            } else if (currentScroll > this.scroll) {
                this.sticking = false;
            }
            this.scroll = currentScroll;
        },
        logout: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee() {
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.next = 2;
                                return this.$auth.logout();

                            case 2:
                                this.$forceUpdate();
                                this.$redirect({ name: 'home' });

                            case 4:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function logout() {
                return _ref.apply(this, arguments);
            }

            return logout;
        }()
    },
    mounted: function mounted() {
        window.addEventListener('scroll', _.throttle(this.stickHeader, 100));
    }
});

/***/ }),

/***/ 95:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "flex-grow flex flex-col" },
    [
      _c("notifications", {
        attrs: {
          position: "top left",
          classes: "notification-top sm:w-80",
          width: "100%"
        }
      }),
      _vm._v(" "),
      _c(
        "nav",
        {
          ref: "navbar",
          staticClass: "z-10 fixed pin-x pin-t flex-none shadow bg-white",
          class: _vm.sticking ? "navbar-stick" : "navbar-unstick",
          attrs: { id: "navbar" }
        },
        [
          _c(
            "div",
            {
              staticClass:
                "container mx-auto px-4 h-14 md:h-20 md:flex md:justify-between"
            },
            [
              _c(
                "div",
                {
                  staticClass:
                    "h-14 md:h-20 flex-grow flex items-stretch justify-between"
                },
                [
                  _c(
                    "router-link",
                    {
                      staticClass:
                        "flex-none flex items-center px-4 -ml-4 text-2xl text-brand font-bold",
                      attrs: { to: { name: "home" } }
                    },
                    [
                      _c("span", { staticClass: "hidden md:inline" }, [
                        _vm._v("Lazy Line")
                      ]),
                      _vm._v(" "),
                      _c("span", { staticClass: "inline md:hidden" }, [
                        _vm._v("LL")
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "flex-grow" },
                    [_c("global-search")],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "button",
                    {
                      staticClass:
                        "flex-none md:hidden flex items-center -mr-4 px-4",
                      attrs: { id: "navbar-toggle", type: "button" },
                      on: {
                        click: function($event) {
                          _vm.toggleNavbar()
                        }
                      }
                    },
                    [
                      _c("i", {
                        staticClass: "ti-menu text-2xl",
                        attrs: { id: "navbar-toggle-open" }
                      }),
                      _vm._v(" "),
                      _c("i", {
                        staticClass: "hidden ti-close text-2xl",
                        attrs: { id: "navbar-toggle-close" }
                      })
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "hidden font-semibold md:flex md:items-stretch",
                  attrs: { id: "navbar-menu" }
                },
                [
                  _c(
                    "router-link",
                    {
                      staticClass:
                        "block md:mx-4 md:flex md:items-center hover:text-brand",
                      attrs: { to: { name: "hello" } },
                      nativeOn: {
                        click: function($event) {
                          _vm.toggleNavbar(false)
                        }
                      }
                    },
                    [_vm._v("\n                    Hello\n                ")]
                  ),
                  _vm._v(" "),
                  [
                    _vm.$auth.check()
                      ? _c(
                          "a",
                          {
                            staticClass:
                              "block md:mx-4 md:flex md:items-center hover:text-brand",
                            attrs: { href: "#" },
                            on: {
                              click: function($event) {
                                $event.preventDefault()
                                _vm.toggleNavbar(false), _vm.logout()
                              }
                            }
                          },
                          [
                            _vm._v(
                              "\n                        Logout\n                    "
                            )
                          ]
                        )
                      : _c(
                          "router-link",
                          {
                            staticClass:
                              "block md:mx-4 md:flex md:items-center hover:text-brand",
                            attrs: { to: { name: "login" } },
                            nativeOn: {
                              click: function($event) {
                                _vm.toggleNavbar(false)
                              }
                            }
                          },
                          [
                            _vm._v(
                              "\n                        Login\n                    "
                            )
                          ]
                        )
                  ]
                ],
                2
              )
            ]
          )
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "flex-grow flex flex-col", attrs: { id: "page" } },
        [
          _c(
            "main",
            { staticClass: "flex flex-grow flex-col", attrs: { id: "main" } },
            [_c("router-view")],
            1
          ),
          _vm._v(" "),
          _vm._m(0)
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "footer",
      { staticClass: "flex-none border-t", attrs: { id: "footer" } },
      [
        _c("div", { staticClass: "container mx-auto px-4 py-2 text-sm" }, [
          _vm._v("\n                © Lazy Line 2018\n            ")
        ])
      ]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-fb5c83d2", module.exports)
  }
}

/***/ })

});
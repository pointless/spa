import { auth, guest } from './middleware/check-auth'
import middleware from './middleware'

export default [
    {
        path: '/',
        name: 'home',
        component: () => import('./views/Home')
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('./views/Login'),
        beforeEnter: middleware([guest])
    },
    {
        path: '/hello',
        name: 'hello',
        component: () => import('./views/Hello'),
        beforeEnter: middleware([auth])
    },
    { 
        path: "*", 
        component: () => import('./views/NotFound')
    }
]
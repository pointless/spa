<?php

namespace App\Exceptions;

use Exception;
use Pointless\Restful\Http\JsonResponse;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{

    protected $dontReport = [
        //
    ];

    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }

    protected function prepareJsonResponse($request, Exception $exception)
    {
        return JsonResponse::error($exception);
    }

    protected function invalidJson($request, ValidationException $exception)
    {
        return JsonResponse::invalid($exception);
    }
    
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return ($request->expectsJson() || $request->is('api/*'))
            ? response()->json(['status' => 'unauthenticated', 'message' => $exception->getMessage()], 401)
            : redirect()->guest(route('login'));
    }
}

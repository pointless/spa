import inViewport from 'in-viewport'
import { redirect } from './libs/helpers'
import api from './libs/api'
import auth from './libs/auth'
import _ from 'lodash'

export default {
    
    install(Vue, options) {
        
        Vue.prototype._ = window._ = _
        Vue.prototype.console = console
        Vue.prototype.$redirect = redirect
        Vue.prototype.$api = api
        Vue.prototype.$auth = auth

        Vue.directive('inserted', {
            inserted(el, binding) {
                binding.value(el)
            }
        })

        Vue.directive('in-viewport', {
            inserted(el, binding) {
                let debounce = binding.arg,
                    container = _.get(binding, 'modifiers.parent', false) ? el.parentElement : undefined
                inViewport(el, { debounce, container }, () => binding.value(el))
            },
            update(el, binding) {
                let debounce = binding.arg,
                    container = _.get(binding, 'modifiers.parent', false) ? el.parentElement : undefined
                inViewport(el, { debounce, container }, () => binding.value(el))
            }
        })

        Vue.component('toggle', require('./components/Toggle.vue'));
        Vue.component('api-form', require('./components/ApiForm.vue'));
        Vue.component('fetch-listing', require('./components/FetchListing.vue'));

    }

}

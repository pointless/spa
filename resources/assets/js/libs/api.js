import axios from 'axios'
import auth from './auth'
import { redirect } from './helpers'

class Api {

    constructor() {
        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
        axios.defaults.paramsSerializer = require('qs').stringify
        
        axios.interceptors.request.use(request => {
            if (auth.token) {
                request.headers['Authorization'] = `Bearer ${auth.token.access_token}`
            }
            return request
        })

        axios.interceptors.response.use(response => response, async error => {
            let { status } = error.response
            if (status === 401 && auth.token) {
                try {
                    await auth.refreshToken()
                } catch (error) {
                    redirect({ name: 'login' })
                    return Promise.reject(error)
                }
                return axios.request(error.config)
            }
            return Promise.reject(error)
        })
    }

    request(method, url, options = {}) {
        return axios(_.merge({ method, url }, options))
    }

    get(url, params, options = {}) {
        return this.request('get', url, _.merge({ params }, options))
    }

    post(url, data, options = {}) {
        return this.request('post', url, _.merge({ data }, options))
    }

    put(url, data, options = {}) {
        return this.request('put', url, _.merge({ data }, options))
    }

    patch(url, data, options = {}) {
        return this.request('patch', url, _.merge({ data }, options))
    }

    delete(url, data, options = {}) {
        return this.request('delete', url, _.merge({ data }, options))
    }

}

const api = new Api

export default api
webpackJsonp([0],{

/***/ 70:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(74)
/* template */
var __vue_template__ = __webpack_require__(75)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/views/Login.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-33212926", Component.options)
  } else {
    hotAPI.reload("data-v-33212926", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__libs_auth__ = __webpack_require__(4);


function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    methods: {
        saveTokenAndRedirect: function () {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee(_ref) {
                var token = _ref.data;
                return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.next = 2;
                                return __WEBPACK_IMPORTED_MODULE_1__libs_auth__["a" /* default */].setToken(token);

                            case 2:
                                _context.next = 4;
                                return __WEBPACK_IMPORTED_MODULE_1__libs_auth__["a" /* default */].initialize();

                            case 4:
                                this.$redirect({ name: 'hello' });

                            case 5:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function saveTokenAndRedirect(_x) {
                return _ref2.apply(this, arguments);
            }

            return saveTokenAndRedirect;
        }()
    }
});

/***/ }),

/***/ 75:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass:
        "block bg-white shadow border-t-6 border-brand container max-w-sm mx-auto"
    },
    [
      _c(
        "div",
        { staticClass: "text-right" },
        [
          _c(
            "router-link",
            {
              staticClass: "inline-block bg-brand text-white px-4 py-1 text-sm",
              attrs: { to: { name: "register" } }
            },
            [_vm._v("\n            New User\n        ")]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("api-form", {
        staticClass: "px-14 py-8",
        attrs: { method: "POST", action: "/api/auth/login" },
        on: { success: _vm.saveTokenAndRedirect },
        scopedSlots: _vm._u([
          {
            key: "default",
            fn: function(form) {
              return _c("div", {}, [
                _c("h2", { staticClass: "text-brand mb-6" }, [
                  _vm._v("Login to your account")
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "mb-6" }, [
                  _c("input", {
                    staticClass: "form-control",
                    attrs: {
                      id: "email",
                      type: "email",
                      name: "email",
                      placeholder: "Email",
                      autofocus: "",
                      autocomplete: "username"
                    }
                  }),
                  _vm._v(" "),
                  form.errorBag.has("email")
                    ? _c("p", {
                        staticClass: "text-red",
                        domProps: {
                          textContent: _vm._s(form.errorBag.first("email"))
                        }
                      })
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "mb-6" }, [
                  _c("input", {
                    staticClass: "form-control",
                    attrs: {
                      id: "password",
                      type: "password",
                      name: "password",
                      placeholder: "Password",
                      autocomplete: "current-password"
                    }
                  }),
                  _vm._v(" "),
                  form.errorBag.has("password")
                    ? _c("p", {
                        staticClass: "text-red",
                        domProps: {
                          textContent: _vm._s(form.errorBag.first("password"))
                        }
                      })
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn btn-block btn-brand",
                    class: { "btn-loading": form.loading },
                    attrs: { type: "submit" }
                  },
                  [_vm._v("\n                Login\n            ")]
                )
              ])
            }
          }
        ])
      }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "bg-grey-lighter text-center p-3 text-sm" },
        [
          _c(
            "router-link",
            {
              staticClass: "link",
              attrs: { to: { name: "password.request" } }
            },
            [_vm._v("Forgot your password?")]
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-33212926", module.exports)
  }
}

/***/ })

});
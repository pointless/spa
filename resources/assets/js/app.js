import Vue from 'vue'
import AppPlugin from './plugin'
import router from './router'
import App from './views/App'
import auth from './libs/auth'

Vue.use(AppPlugin)

auth.initialize().then(() => {

    window.vm = new Vue({
        el: '#app',
        components: { App },
        router,
    }).$mount('#app')
    
})

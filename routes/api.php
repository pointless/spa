<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name('api.')->namespace('\Api')->group(function () {

    Route::group([], function () {
        Route::post('auth/login', 'AuthController@login');
        Route::post('auth/refresh', 'AuthController@refresh');
        
        // Route::post('register', 'Auth\RegisterController@register');
        // Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
        // Route::post('password/reset', 'Auth\ResetPasswordController@reset');

        // Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
        // Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
    });

    Route::middleware('auth:api,web')->group(function () {
        Route::get('auth/user', 'AuthController@user');
        Route::post('auth/logout', 'AuthController@logout');
    });
});

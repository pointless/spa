<?php

use Monolog\Logger;
use Illuminate\Support\Carbon;
use Monolog\Handler\StreamHandler;

define('DATE_FORMAT', 'j M Y');
define('DATETIME_FORMAT', 'j M Y H:i');

if (!function_exists('dblisten')) {
    function dblisten($log = null)
    {
        $logger = false;
        if ($log) {
            $logfile = storage_path('logs/' . (is_string($log) ? $log : 'query.log'));
            File::delete($logfile);
            $logger = new Logger('Query');
            $logger->pushHandler(new StreamHandler($logfile));
        }

        DB::listen(function ($query) use ($logger) {
            if ($logger) {
                $logger->addDebug($query->sql, [$query->bindings, $query->time]);
            } else {
                dump($query->sql, $query->bindings, $query->time);
            }
        });
    }
}

if (!function_exists('start_case')) {
    function start_case($value)
    {
        return title_case(snake_case(studly_case($value), ' '));
    }
}

if (!function_exists('upper_case')) {
    function upper_case($value)
    {
        return strtoupper(start_case($value));
    }
}

if (!function_exists('lower_case')) {
    function lower_case($value)
    {
        return strtolower(start_case($value));
    }
}

if (!function_exists('pr')) {
    function pr(...$expressions)
    {
        if ('cli' !== PHP_SAPI) {
            echo '<pre>';
        }

        array_map('print_r', $expressions);

        if ('cli' !== PHP_SAPI) {
            echo '</pre>';
        }
    }
}

if (!function_exists('vd')) {
    function vd(...$expressions)
    {
        if ('cli' !== PHP_SAPI) {
            echo '<pre>';
        }

        var_dump(...$expressions);

        if ('cli' !== PHP_SAPI) {
            echo '</pre>';
        }
    }
}

if (!function_exists('prd')) {
    function prd(...$expressions)
    {
        pr(...$expressions);
        die(1);
    }
}

if (!function_exists('vdd')) {
    function vdd(...$expressions)
    {
        vd(...$expressions);
        die(1);
    }
}

if (!function_exists('carbon')) {
    function carbon($date, $tz = null)
    {
        return new Carbon($date, $tz);
    }
}

if (!function_exists('carbon_range')) {
    function carbon_range($from, $to)
    {
        $from = carbon($from);
        $to = carbon($to);
        $totalDays = $from->diffInDays($to);
        return collect()->times($totalDays + 1, function ($days) use ($from) {
            return $from->copy()->addDays($days - 1);
        });
    }
}

if (!function_exists('format_date')) {
    function format_date($date, $format = DATE_FORMAT)
    {
        return $date ? Illuminate\Support\Carbon::parse($date)->format($format) : null;
    }
}

if (!function_exists('format_datetime')) {
    function format_datetime($date, $format = DATETIME_FORMAT)
    {
        return $date ? Illuminate\Support\Carbon::parse($date)->format($format) : null;
    }
}

if (!function_exists('number_ordinal')) {
    function number_ordinal($num)
    {
        if (!in_array(($num % 100), [11, 12, 13])) {
            switch ($num % 10) {
                case 1:
                    return $num . 'st';
                case 2:
                    return $num . 'nd';
                case 3:
                    return $num . 'rd';
            }
        }
        return $num . 'th';
    }
}

if (!function_exists('uclower')) {
    function uclower($string)
    {
        return ucwords(strtolower($string));
    }
}

if (!function_exists('explode_filter')) {
    function explode_filter($seperator, $string)
    {
        return array_filter(array_map('trim', explode($seperator, $string)));
    }
}

if (!function_exists('class_uses_trait')) {
    function class_uses_trait($class, $trait)
    {
        return in_array($trait, class_uses_recursive($class));
    }
}

if (!function_exists('htmlattributes')) {
    function htmlattributes($attributes)
    {
        $html = [];
        foreach ((array)$attributes as $key => $value) {
            if (is_numeric($key)) {
                $key = $value;
            }

            if (!is_null($value)) {
                if ($key === 'class' && is_array($value)) {
                    $value = implode(' ', $value);
                } elseif ($key === 'style' && is_array($value)) {
                    $value = collect($value)->map(function ($val, $key) {
                        return is_null($val) ? null : $key . ':' . $val;
                    })->filter()->implode(';');
                }
                if (is_array($value) || is_object($value)) {
                    $value = json_encode($value);
                }
                $html[] = $key . '="' . htmlentities($value, ENT_QUOTES, 'UTF-8') . '"';
            }
        }
        return count($html) > 0 ? ' ' . implode(' ', $html) : '';
    }
}

import localForage from 'localforage'
import api from './api'

class Auth {

    async initialize() {
        this.token = await this.getToken()
        if (this.token) {
            this.user = await this.fetchUser()
        }
    }

    setToken(token) {
        this.token = token
        return localForage.setItem('token', token)
    }
    
    getToken() {
        return localForage.getItem('token')
    }

    async fetchUser() {
        try {
            const response = await api.get('/api/auth/user')
            const { data: user } = response.data
            return user
        } catch (error) {
            console.log(error)
        }
    }

    async refreshToken() {
        this.token = undefined
        const response = await api.post('/api/auth/refresh')
        const { data: token } = response.data
        return this.setToken(token)
    }

    check() {
        return !! this.user
    }

    async logout() {
        try {
            await api.post('/api/auth/logout')
        } catch (error) {
            console.log(error)
        }
        this.token = undefined
        this.user = undefined
        return localForage.removeItem('token')
    }

}

const auth = new Auth

export default auth
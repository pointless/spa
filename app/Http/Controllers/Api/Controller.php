<?php

namespace App\Http\Controllers\Api;

use App\Http\JsonResponse;
use Pointless\Restful\Http\Traits\ReturnsJsonResponse;
use App\Http\Controllers\Controller as BaseController;

abstract class Controller extends BaseController
{
    use ReturnsJsonResponse;

}

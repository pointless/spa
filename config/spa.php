<?php

return [

    'client_id' => env('SPA_CLIENT_ID'),
    
    'client_secret' => env('SPA_CLIENT_SECRET'),


];
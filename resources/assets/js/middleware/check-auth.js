import authLib from '../libs/auth'


export async function auth(to, from, next) {
    if (authLib.check()) {
        next()
    } else {
        next({ name: 'login' })
    }
}


export async function guest(to, from, next) {
    if (! authLib.check()) {
        next()
    } else {
        next({ name: 'home' })
    }
}